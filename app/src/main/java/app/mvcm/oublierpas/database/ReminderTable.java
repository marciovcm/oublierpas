package app.mvcm.oublierpas.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class ReminderTable {

    // Database table
    public static final String TABLE_REMINDERS = "reminders";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TITLE = "task_title";
    public static final String COLUMN_SUMMARY = "task_summary";
    public static final String COLUMN_DATETIME = "date_time";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_REPEAT = "task_repeat";
    public static final String COLUMN_STATUS = "task_status";

    public static final int COLUMN_ID_IDX = 0;
    public static final int COLUMN_TITLE_IDX = 1;
    public static final int COLUMN_SUMMARY_IDX = 2;
    public static final int COLUMN_DATETIME_IDX = 3;
    public static final int COLUMN_ADDRESS_IDX = 4;
    public static final int COLUMN_REPEAT_IDX = 5;
    public static final int COLUMN_STATUS_IDX = 6;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "CREATE TABLE " + TABLE_REMINDERS
            + "( "
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_TITLE + " TEXT NOT NULL, "
            + COLUMN_SUMMARY + " TEXT, "
            + COLUMN_DATETIME + " TEXT, "
            + COLUMN_ADDRESS + " TEXT, "
            + COLUMN_REPEAT + " TEXT, "
            + COLUMN_STATUS + " TEXT NOT NULL"
            + " ) ";

    public static void onCreate(SQLiteDatabase database){
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion){
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_REMINDERS);
        onCreate(database);
    }
}
