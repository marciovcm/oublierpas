package app.mvcm.oublierpas.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class ReminderDBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "oublierpas.db";
    private static final int DATABASE_VERSION = 1;

    public ReminderDBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Method is called during creation of the database
    @Override
    public void onCreate(SQLiteDatabase db) {
        ReminderTable.onCreate(db);

//        db.execSQL(ReminderTable.insertReminder("\"Tarefa 1\"", null, "\"2016-01-01 08:00\"", null, "\"none\"", "\"completed\""));
//        db.execSQL(ReminderTable.insertReminder("\"Tarefa 2\"", null, "\"2016-05-20 09:00\"", null, "\"weekly\"", "\"completed\""));
//        db.execSQL(ReminderTable.insertReminder("\"Tarefa 3\"", "\"CPFL\"", "\"2016-03-15 07:00\"", null, "\"monthly\"", "\"due\""));
//        db.execSQL(ReminderTable.insertReminder("\"Tarefa 4\"", "\"IPVA\"", "\"2016-04-10 06:00\"", null, "\"yearly\"", "\"ongoing\""));
//        db.execSQL(ReminderTable.insertReminder("\"Tarefa 5\"", "\"IPVA\"", "\"2016-02-05 05:00\"", null, "\"yearly\"", "\"ongoing\""));

    }

    // Method is called during an upgrade of the database,
    // e.g. if you increase the database version
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        ReminderTable.onUpgrade(db, oldVersion, newVersion);
    }
}
