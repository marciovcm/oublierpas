package app.mvcm.oublierpas.contentprovider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.HashSet;

import app.mvcm.oublierpas.database.ReminderDBHelper;
import app.mvcm.oublierpas.database.ReminderTable;


public class ReminderContentProvider extends ContentProvider {

    // database
    private ReminderDBHelper database;

    // used for UriMatcher
    private static final int REMINDERS = 10;
    private static final int REMINDERS_ID = 20;

    private static final String AUTHORITY = "app.mvcm.oublierpas";

    private static final String BASE_PATH = "reminders";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH, REMINDERS);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", REMINDERS_ID);
    }

    @Override
    public boolean onCreate() {
        database = new ReminderDBHelper(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        // using SQLiteQueryBuilder instead of query() method
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        // check if the caller has requested a column which does not exists
        checkColumns(projection);

        // set the table
        queryBuilder.setTables(ReminderTable.TABLE_REMINDERS);

        int uriType = sURIMatcher.match(uri);
        switch (uriType){
            case REMINDERS:
                break;
            case REMINDERS_ID:
                // adding the ID to the original query
                queryBuilder.appendWhere(ReminderTable.COLUMN_ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        // make sure that potential listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {

        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        long id = 0;

        switch (uriType){
            case REMINDERS:
                id = sqlDB.insert(ReminderTable.TABLE_REMINDERS, null, values);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsDeleted = 0;

        switch (uriType){
            case REMINDERS:
                rowsDeleted = sqlDB.delete(ReminderTable.TABLE_REMINDERS, selection, selectionArgs);
                break;
            case REMINDERS_ID:
                String id = uri.getLastPathSegment();
                if(TextUtils.isEmpty(selection)){
                    rowsDeleted = sqlDB.delete(ReminderTable.TABLE_REMINDERS, ReminderTable.COLUMN_ID + "=" + id, null);
                }else{
                    rowsDeleted = sqlDB.delete(ReminderTable.TABLE_REMINDERS, ReminderTable.COLUMN_ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsUpdated = 0;

        switch(uriType){
            case REMINDERS:
                rowsUpdated = sqlDB.update(ReminderTable.TABLE_REMINDERS, values, selection, selectionArgs);
                break;
            case REMINDERS_ID:
                String id = uri.getLastPathSegment();
                if(TextUtils.isEmpty(selection)){
                    rowsUpdated = sqlDB.update(ReminderTable.TABLE_REMINDERS, values, ReminderTable.COLUMN_ID + "=" + id, null);
                }else{
                    rowsUpdated = sqlDB.update(ReminderTable.TABLE_REMINDERS, values, ReminderTable.COLUMN_ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return rowsUpdated;
    }

    private void checkColumns(String[] projection){

        String[] available = { ReminderTable.COLUMN_STATUS, ReminderTable.COLUMN_REPEAT, ReminderTable.COLUMN_ADDRESS
                , ReminderTable.COLUMN_DATETIME, ReminderTable.COLUMN_SUMMARY
                , ReminderTable.COLUMN_TITLE, ReminderTable.COLUMN_ID };

        if(projection != null){
            HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
            if(!availableColumns.containsAll(requestedColumns)){
                throw new IllegalArgumentException("Unknown columns in projection");
            }
        }

    }

}
