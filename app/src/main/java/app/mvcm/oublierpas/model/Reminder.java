package app.mvcm.oublierpas.model;


public class Reminder {

    private int id;
    private static String title;
    private static String summary;
    private static String date;
    private static String hour;
    private static String repeat;
    private static String address;
    private static String status;
    private static String reminderType;

    public Reminder(){
        id = 0;
        title = null;
        summary = null;
        date = null;
        hour = null;
        repeat = null;
        address = null;
        status = null;
        reminderType = null;
    }

    public Reminder(String type){
        id = getId();
        title = getTitle();
        summary = getSummary();
        date = getDate();
        hour = getHour();
        repeat = getRepeat();
        address = getAdress();
        status = getStatus();
        reminderType = type;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getSummary() {
        return summary;
    }

    public String getDate() {
        return date;
    }

    public String getHour() {
        return hour;
    }

    public String getRepeat() {
        return repeat;
    }

    public String getAdress() {
        return address;
    }

    public String getStatus() {
        return status;
    }

    public String getReminderType() {
        return reminderType;
    }

    public void setId(int id) { this.id = id; }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setReminderType(String reminderType) { this.reminderType = reminderType; }
}
