package app.mvcm.oublierpas.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toolbar;

import app.mvcm.oublierpas.R;
import app.mvcm.oublierpas.contentprovider.ReminderContentProvider;
import app.mvcm.oublierpas.database.ReminderTable;
import app.mvcm.oublierpas.model.Reminder;
import app.mvcm.oublierpas.view.pickers.FragmentDatePicker;
import app.mvcm.oublierpas.view.pickers.FragmentRepeatPicker;
import app.mvcm.oublierpas.view.pickers.FragmentTimePicker;

public class ReminderInfoActivity extends Activity implements View.OnClickListener{

    private String TASK_TYPE;
    private String TASK_MANAGER;

    private DialogFragment date_picker;
    private DialogFragment time_picker;
    private DialogFragment repeat_picker;
    private Reminder mReminder = new Reminder();

    private ImageButton time_button;
    private ImageButton place_button;

    private Toolbar toolbar;
    private EditText task_title;
    private EditText task_date;
    private EditText task_hour;
    private EditText task_repeat;
    private EditText task_address;
    private EditText task_summary;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_info);
        TASK_MANAGER = getIntent().getStringExtra("task_manager");

        toolbar = (Toolbar) findViewById(R.id.reminder_info_toolbar);
        toolbar.setTitle(getResources().getString(R.string.str_reminder_info_toolbar));
        toolbar.setTitleTextColor(getResources().getColor(R.color.reminder_toolbar_title_color));
        setActionBar(toolbar);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(true);

        Typeface mTypeface = Typeface.createFromAsset(getAssets(),"font/Roboto-Light.ttf");

        time_button = (ImageButton) findViewById(R.id.reminder_time_button);
        time_button.setOnClickListener(this);
        place_button = (ImageButton) findViewById(R.id.reminder_place_button);
        place_button.setOnClickListener(this);

        changeButtonFocus("time");

        task_title = (EditText) findViewById(R.id.task_title);
        task_date = (EditText) findViewById(R.id.task_date);
        task_hour = (EditText) findViewById(R.id.task_time);
        task_repeat = (EditText) findViewById(R.id.task_repeat);
        task_address = (EditText) findViewById(R.id.task_place);
        task_summary = (EditText) findViewById(R.id.task_summary);

        task_date.setTypeface(mTypeface);
        task_date.setOnClickListener(this);
        task_hour.setTypeface(mTypeface);
        task_hour.setOnClickListener(this);
        task_repeat.setTypeface(mTypeface);
        task_repeat.setOnClickListener(this);
        task_address.setTypeface(mTypeface);
        task_summary.setTypeface(mTypeface);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.reminder_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                finish();
                break;
            case R.id.action_confirm:
                boolean isEmpty = checkEmptyField();
                if(!isEmpty){
                    getReminderInfo(TASK_MANAGER);
                }
                break;
            case R.id.action_delete:
                if(TASK_MANAGER.equals("new_task")){
                    finish();
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.reminder_time_button:
                changeButtonFocus("time");
                changeTaskVisibility("time");
                break;
            case R.id.reminder_place_button:
                changeButtonFocus("place");
                changeTaskVisibility("place");
                break;
            case R.id.task_date:
                date_picker = new FragmentDatePicker();
                date_picker.setCancelable(false);
                date_picker.show(getFragmentManager(), "date picker");
                break;
            case R.id.task_time:
                time_picker = new FragmentTimePicker();
                time_picker.setCancelable(false);
                time_picker.show(getFragmentManager(), "time picker");
                break;
            case R.id.task_repeat:
                repeat_picker = new FragmentRepeatPicker();
                repeat_picker.setCancelable(false);
                repeat_picker.show(getFragmentManager(), "repeat picker");
                break;
        }

    }

    private void changeButtonFocus(String which){

        if(which.equals("time")){
            time_button.setColorFilter(getResources().getColor(R.color.colorAccent));
            time_button.setImageAlpha(255);
            place_button.setColorFilter(R.color.reminder_toolbar_icon);
            place_button.setImageAlpha(30);
        }else if(which.equals("place")){
            place_button.setColorFilter(getResources().getColor(R.color.colorAccent));
            place_button.setImageAlpha(255);
            time_button.setColorFilter(R.color.reminder_toolbar_icon);
            time_button.setImageAlpha(30);
        }

        TASK_TYPE = which;
        mReminder.setReminderType(which);

    }

    private void changeTaskVisibility(String which){

        if(which.equals("time")){
            task_date.setVisibility(View.VISIBLE);
            findViewById(R.id.separator_2).setVisibility(View.VISIBLE);
            task_hour.setVisibility(View.VISIBLE);
            findViewById(R.id.separator_3).setVisibility(View.VISIBLE);
            findViewById(R.id.star_3).setVisibility(View.VISIBLE);
            task_repeat.setVisibility(View.VISIBLE);
            findViewById(R.id.separator_4).setVisibility(View.VISIBLE);
            findViewById(R.id.star_4).setVisibility(View.VISIBLE);
            task_address.setVisibility(View.GONE);
            findViewById(R.id.separator_5).setVisibility(View.GONE);
            TASK_TYPE = which;
        }else if(which.equals("place")){
            task_date.setVisibility(View.GONE);
            findViewById(R.id.separator_2).setVisibility(View.GONE);
            task_hour.setVisibility(View.GONE);
            findViewById(R.id.separator_3).setVisibility(View.GONE);
            findViewById(R.id.star_3).setVisibility(View.GONE);
            task_repeat.setVisibility(View.GONE);
            findViewById(R.id.separator_4).setVisibility(View.GONE);
            findViewById(R.id.star_4).setVisibility(View.GONE);
            task_address.setVisibility(View.VISIBLE);
            findViewById(R.id.separator_5).setVisibility(View.VISIBLE);
            TASK_TYPE = which;
        }

    }

    public boolean checkEmptyField(){

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getResources().getString(R.string.str_reminder_info_dialog_title));
        dialog.setMessage(getResources().getString(R.string.str_reminder_info_dialog_message));
        dialog.setPositiveButton("OK", null);

        if(TASK_TYPE.equals("time")){
            if(TextUtils.isEmpty(task_title.getText()) || TextUtils.isEmpty(task_date.getText())
                    || TextUtils.isEmpty(task_hour.getText()) || TextUtils.isEmpty(task_repeat.getText())){
                dialog.setCancelable(false);
                dialog.show();
                return true;
            }
        }else{
            if(TextUtils.isEmpty(task_title.getText()) || TextUtils.isEmpty(task_address.getText())){
                dialog.setCancelable(false);
                dialog.show();
                return true;
            }
        }

        return false;

    }

    public void getReminderInfo(String manager){

        ContentValues values = new ContentValues();

        mReminder.setTitle(task_title.getText().toString());
        mReminder.setSummary(task_summary.getText().toString());

        values.put("task_title", mReminder.getTitle());
        values.put("task_summary", mReminder.getSummary());
        values.put("date_time",mReminder.getDate() + " " + mReminder.getHour());
        values.put("address", mReminder.getAdress());
        values.put("task_repeat", mReminder.getRepeat());
        values.put("task_status", "ongoing");

        if(manager.equals("new_task")){
            getContentResolver().insert(ReminderContentProvider.CONTENT_URI, values);
            finish();
        }else if(manager.equals("update_task")){
//            provider.update();
        }

    }

}
