package app.mvcm.oublierpas.view.pickers;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

import app.mvcm.oublierpas.R;
import app.mvcm.oublierpas.model.Reminder;


public class FragmentDatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private Reminder mReminder;
    private EditText date_picker;
    private String[] month_array;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        mReminder = new Reminder("time");

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        // Do something with the date chosen by the user
        month_array = getActivity().getResources().getStringArray(R.array.months);
        mReminder.setDate(year + "-" + (month+1) + "-" + dayOfMonth);

        date_picker = (EditText) getActivity().findViewById(R.id.task_date);
        date_picker.setText(month_array[month] + " " + dayOfMonth + ", " + year);
    }
}
