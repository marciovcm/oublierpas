package app.mvcm.oublierpas.view;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toolbar;

import app.mvcm.oublierpas.R;
import app.mvcm.oublierpas.database.ReminderDBHelper;

public class MainActivity extends Activity implements View.OnClickListener{

    private Toolbar toolbar;
    private FragmentTransaction transaction;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor editor;

    private FragmentReminderTime mFragmentReminderTime;
    private FragmentReminderPlace mFragmentReminderPlace;

    ImageButton time_button;
    ImageButton place_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.top_toolbar);
        toolbar.setTitle("");
        setActionBar(toolbar);

        mFragmentReminderTime = new FragmentReminderTime();
        mFragmentReminderPlace = new FragmentReminderPlace();

        time_button = (ImageButton) findViewById(R.id.reminder_time_button);
        place_button = (ImageButton) findViewById(R.id.reminder_place_button);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = mSharedPreferences.edit();

        if(mSharedPreferences.getString("homepage", null) == null){
            replaceFragment(null);
        }else if (mSharedPreferences.getString("homepage", null).contains("time")){
            replaceFragment(mFragmentReminderTime);
        }else if (mSharedPreferences.getString("homepage", null).contains("place")){
            replaceFragment(mFragmentReminderPlace);
        }

        time_button.setOnClickListener(this);
        place_button.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.reminder_time_button:
                replaceFragment(mFragmentReminderTime);
                break;
            case R.id.reminder_place_button:
                replaceFragment(mFragmentReminderPlace);
                break;
        }
    }

    private void replaceFragment(Fragment fragment){

        transaction = getFragmentManager().beginTransaction();

        if(fragment == null){
            transaction.add(R.id.main_frame, mFragmentReminderTime);
            changeButtonFocus(null);
        }else if(fragment instanceof FragmentReminderTime){
            transaction.replace(R.id.main_frame, mFragmentReminderTime);
            changeButtonFocus(mFragmentReminderTime);
        }else if(fragment instanceof FragmentReminderPlace){
            transaction.replace(R.id.main_frame, mFragmentReminderPlace);
            changeButtonFocus(mFragmentReminderPlace);
        }

        transaction.commit();
    }

    private void changeButtonFocus(Fragment fragment){

        if(fragment == null || (fragment instanceof FragmentReminderTime)){
            time_button.setColorFilter(getResources().getColor(R.color.colorAccent));
            time_button.setImageAlpha(255);
            place_button.setColorFilter(R.color.reminder_toolbar_icon);
            place_button.setImageAlpha(30);
        }else if(fragment instanceof FragmentReminderPlace){
            place_button.setColorFilter(getResources().getColor(R.color.colorAccent));
            place_button.setImageAlpha(255);
            time_button.setColorFilter(R.color.reminder_toolbar_icon);
            time_button.setImageAlpha(30);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        switch (id){
            case R.id.action_add_reminder:
                intent = new Intent(this, ReminderInfoActivity.class);
                intent.putExtra("task_manager", "new_task");
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
