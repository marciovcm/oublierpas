package app.mvcm.oublierpas.view.pickers;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.EditText;

import app.mvcm.oublierpas.R;
import app.mvcm.oublierpas.model.Reminder;


public class FragmentRepeatPicker extends DialogFragment {

    private EditText repeat_picker;
    private String[] repeat_array;
    private Reminder mReminder;
    private String str_repeat;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());

        repeat_picker = (EditText) getActivity().findViewById(R.id.task_repeat);
        repeat_array = getActivity().getResources().getStringArray(R.array.alarm_repeat);

        mReminder = new Reminder("time");

        dialog.setTitle(R.string.str_reminder_info_repeat);
        dialog.setItems(R.array.alarm_repeat, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                str_repeat = repeat_array[which];
                repeat_picker.setText(str_repeat);
                mReminder.setRepeat(str_repeat);
            }
        });

        return dialog.create();
    }
}
