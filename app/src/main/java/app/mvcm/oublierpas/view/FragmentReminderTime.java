package app.mvcm.oublierpas.view;


import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import app.mvcm.oublierpas.R;
import app.mvcm.oublierpas.adapter.ReminderTimeAdapter;
import app.mvcm.oublierpas.contentprovider.ReminderContentProvider;
import app.mvcm.oublierpas.database.ReminderDBHelper;

public class FragmentReminderTime extends Fragment{

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor editor;
    private ReminderDBHelper helper;
    private SQLiteDatabase db;
    private Cursor cursor;
    private ListView list;
    private ReminderTimeAdapter time_adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reminder_time, container, false);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        editor = mSharedPreferences.edit();
        editor.putString("homepage", "time");
        editor.commit();

        cursor = getActivity().getContentResolver().query(ReminderContentProvider.CONTENT_URI, null, null, null, null);
        list = (ListView) view.findViewById(R.id.reminder_time_list);
        time_adapter = new ReminderTimeAdapter(getActivity(), cursor, true);
        list.setAdapter(time_adapter);
        list.setOnItemClickListener(new mOnItemClickListener());
        registerForContextMenu(list);

        return view;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        ImageView status = (ImageView) v.findViewById(R.id.completed_sign);
        inflater.inflate(R.menu.context_menu, menu);

        if(status.getVisibility() == View.VISIBLE){
            menu.findItem(R.id.cm_completed).setVisible(false);
            menu.findItem(R.id.cm_incompleted).setVisible(true);
        }else{
            menu.findItem(R.id.cm_completed).setVisible(true);
            menu.findItem(R.id.cm_incompleted).setVisible(false);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }

    public class mOnItemClickListener implements AdapterView.OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        }
    }


}
