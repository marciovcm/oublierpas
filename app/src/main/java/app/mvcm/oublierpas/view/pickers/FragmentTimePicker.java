package app.mvcm.oublierpas.view.pickers;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TimePicker;
import java.util.Calendar;

import app.mvcm.oublierpas.R;
import app.mvcm.oublierpas.model.Reminder;

public class FragmentTimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    private Reminder mReminder;
    private EditText time_picker;
    private String str_time;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        mReminder = new Reminder("time");

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute, true);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // Do something with the time chosen by the user
        time_picker = (EditText) getActivity().findViewById(R.id.task_time);

        if((hourOfDay) < 10 && (minute < 10)){
            str_time = String.format("%02d",hourOfDay) + ":" + String.format("%02d", minute);
        }else if(hourOfDay < 10){
            str_time = String.format("%02d",hourOfDay) + ":" + minute;
        }else if(minute < 10){
            str_time = hourOfDay + ":" + String.format("%02d", minute);
        }else{
            str_time = hourOfDay + ":" + minute;
        }

        mReminder.setHour(str_time);
        time_picker.setText(str_time);

    }
}
