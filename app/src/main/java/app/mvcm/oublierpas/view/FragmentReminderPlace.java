package app.mvcm.oublierpas.view;


import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.mvcm.oublierpas.R;

public class FragmentReminderPlace extends Fragment{

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reminder_place, container, false);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        editor = mSharedPreferences.edit();

        editor.putString("homepage","place");
        editor.commit();

        return view;
    }
}
