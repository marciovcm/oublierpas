package app.mvcm.oublierpas.adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import app.mvcm.oublierpas.R;
import app.mvcm.oublierpas.database.ReminderTable;

public class ReminderTimeAdapter extends CursorAdapter {

    public ReminderTimeAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        ViewHolder holder = (ViewHolder) view.getTag();

        int task_id = cursor.getInt(ReminderTable.COLUMN_ID_IDX);
        String task_title = cursor.getString(ReminderTable.COLUMN_TITLE_IDX);
        String task_summary = cursor.getString(ReminderTable.COLUMN_SUMMARY_IDX);
        String task_date = cursor.getString(ReminderTable.COLUMN_DATETIME_IDX);
        String task_repeat = cursor.getString(ReminderTable.COLUMN_REPEAT_IDX);
        String task_status = cursor.getString(ReminderTable.COLUMN_STATUS_IDX);

        task_date = getFormattedDate(task_date);

        holder.id.setText("#"+ task_id);
        holder.title.setText(task_title);
        holder.date_hour.setText(task_date);
        if(task_repeat.equals("none")){
            holder.mLinearLayout.setVisibility(View.GONE);
        }else{
            holder.repeat.setText(task_repeat);
            holder.mLinearLayout.setVisibility(View.VISIBLE);
        }
        holder.repeat.setText(task_repeat);
        if(task_status.equals("ongoing")){
            holder.completed_sign.setVisibility(View.GONE);
            holder.due_sign.setVisibility(View.GONE);
            holder.title.setPaintFlags(holder.title.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
            holder.date_hour.setPaintFlags(holder.date_hour.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
            holder.repeat.setPaintFlags(holder.repeat.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
        }else if(task_status.equals("completed")){
            holder.completed_sign.setVisibility(View.VISIBLE);
            holder.due_sign.setVisibility(View.GONE);
            holder.title.setPaintFlags(holder.title.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.date_hour.setPaintFlags(holder.date_hour.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.repeat.setPaintFlags(holder.repeat.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else if(task_status.equals("due")){
            holder.completed_sign.setVisibility(View.GONE);
            holder.due_sign.setVisibility(View.VISIBLE);
            holder.title.setPaintFlags(holder.title.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
            holder.date_hour.setPaintFlags(holder.date_hour.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
            holder.repeat.setPaintFlags(holder.repeat.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
        }

        if(TextUtils.isEmpty(task_summary)){
            holder.summary.setVisibility(View.GONE);
        }else{
            holder.summary.setVisibility(View.VISIBLE);
        }

        view.setId(ReminderTable.COLUMN_ID_IDX);

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_reminder_time, parent, false);

        ViewHolder holder = new ViewHolder(view);
        view.setTag(holder);

        return view;
    }

    
    public static class ViewHolder{

        TextView id;
        TextView title;
        ImageView summary;
        TextView date_hour;
        TextView repeat;
        ImageView completed_sign;
        ImageView due_sign;
        LinearLayout mLinearLayout;

        public ViewHolder(View view){
            id = (TextView) view.findViewById(R.id.reminder_time_id_number);
            title = (TextView) view.findViewById(R.id.reminder_time_title);
            summary = (ImageView) view.findViewById(R.id.summary_info_sign);
            date_hour = (TextView) view.findViewById(R.id.reminder_time_date);
            repeat = (TextView) view.findViewById(R.id.repeat_period);
            completed_sign = (ImageView) view.findViewById(R.id.completed_sign);
            due_sign = (ImageView) view.findViewById(R.id.overdue_sign);
            mLinearLayout = (LinearLayout) view.findViewById(R.id.reminder_time_repetition);
        }
    }

    public String getFormattedDate(String str){

        try {
            SimpleDateFormat original = new SimpleDateFormat("yyyy-MM-dd hh:mm");
            Date date = original.parse(str);
            SimpleDateFormat formatted = new SimpleDateFormat("MMMM dd, yyyy hh:ss");
            str = formatted.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return str;
    }


}
